package model

import "time"

type UserRoleEntity struct {
	RoleNum    string
	RoleName   string
	RoleDes    string
	RoleIdName string
	RoleEnable int
}

type UserRoleInfo struct {
	RoleId     string `gorm:"primary_key"`
	RoleIdName string
	RoleNum    string
	RoleName   string
	RoleDes    string
	RoleEnable int
	CreateTime time.Time
	UpdateTime time.Time
}
