package model

import "time"

type UserInfoEntity struct {
	UserName         string    `json:"userName"`
	UserAvatar       string    `json:"userAvatar"`
	UserEmail        string    `json:"userEmail"`
	UserPhoneNum     string    `json:"userPhoneNum"`
	UserPassword     string    `json:"userPassword"`
	SmsCode          int       `json:"smsCode"`
	Token            string    `json:"token"`
	UserId           string    `json:"userId"`
	LoginMode        string    `json:"loginMode"`
	Permission       string    `json:"permission"`
	UserIntroduction string    `json:"userIntroduction"`
	UserRole         string    `json:"userRole"`
	UserEnable       int       `json:"userEnable"`
	LoginTime        time.Time `json:"loginTime" form:"loginTime"`
}

type UserInfo struct {
	UserId           string    `gorm:"primary_key" json:"userId" form:"user_id"`
	UserName         string    `json:"userName" form:"user_name"`
	UserAvatar       string    `json:"userAvatar"`
	UserEmail        string    `json:"userEmail" form:"user_email"`
	UserPhoneNum     string    `json:"userPhoneNum" form:"user_phone"`
	UserPassword     string    `json:"userPassword" form:"user_password"`
	UserIntroduction string    `json:"userIntroduction"`
	UserRole         string    `json:"userRole"`
	UserEnable       int       `json:"userEnable"`
	Token            string    `json:"token"`
	Permission       string    `json:"permission"`
	UserLoginTime    time.Time `json:"loginTime" form:"loginTime"`
	CreateTime       time.Time `json:"createTime" form:"create_time"`
	UpdateTime       time.Time `json:"updateTime" form:"update_time"`
}
