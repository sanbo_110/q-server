package model

import "time"

type UserNavInfo struct {
	UserNavId  string `gorm:"primary_key"`
	Name       string
	ParentId   int64
	Id         int64
	Redirect   string
	Path       string
	Component  string
	Icon       string
	Title      string
	Show       bool
	CreateTime time.Time
	UpdateTime time.Time
}
