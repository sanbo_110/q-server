package dao

import (
	"accountCenter/db"
	. "accountCenter/model"
	"github.com/go-basic/uuid"
	"log"
	"time"
)

type UserRoleDao struct {
}

// AddUserRole   /*
func (role *UserRoleDao) AddUserRole(entity UserRoleEntity) (roleInfo UserRoleInfo, code int, msg string, err error) {

	roleInfo = UserRoleInfo{
		RoleId:     uuid.New(),
		RoleNum:    entity.RoleNum,
		RoleName:   entity.RoleName,
		RoleDes:    entity.RoleDes,
		RoleEnable: entity.RoleEnable,
		RoleIdName: entity.RoleIdName,
		CreateTime: time.Now(),
		UpdateTime: time.Now(),
	}

	if db.GormDb.HasTable(&UserRoleInfo{}) {
		db.GormDb.AutoMigrate(&UserRoleInfo{})
	} else {
		db.GormDb.CreateTable(&UserRoleInfo{})
	}

	err = db.GormDb.Create(&roleInfo).Error

	if err != nil {
		return roleInfo, 51101, "操作失败，请重试", err
	}

	return roleInfo, 200, "", err
}

// GetOne /*
func (role *UserRoleDao) GetOne(roleId string) (roleInfo UserRoleInfo, err error) {

	err = db.GormDb.Where("role_id = ?", roleId).First(&roleInfo).Error

	if err != nil {
		log.Println(err)
		return roleInfo, err
	}

	return roleInfo, err
}

// GetAll /*
func (role *UserRoleDao) GetAll() (roles []UserRoleInfo, code int, msg string, err error) {

	if err := db.GormDb.Find(&roles).Error; err != nil {
		log.Println(err)
	}

	return roles, 200, "", err
}

// Update /*
func (role *UserRoleDao) Update(entity UserRoleInfo) (roleInfo UserRoleInfo, code int, msg string, err error) {

	roleInfo, err = role.GetOne(entity.RoleId)

	if err != nil {
		log.Println(err)
		return roleInfo, 51201, "操作失败，请重试", err
	}
	// 更新
	{
		if entity.RoleName != "" {
			roleInfo.RoleName = entity.RoleName
		}
		if entity.RoleIdName != "" {
			roleInfo.RoleIdName = entity.RoleIdName
		}
		if entity.RoleDes != "" {
			roleInfo.RoleDes = entity.RoleDes
		}
		roleInfo.RoleEnable = entity.RoleEnable
		err = db.GormDb.Model(&roleInfo).Update(roleInfo).Error
		if err != nil {
			log.Println(err)
			return roleInfo, 51202, "操作失败，请重试", err
		}
	}
	return roleInfo, 20000, "", err
}
