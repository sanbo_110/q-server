package main

import (
	"apiGateway/client"
	_ "apiGateway/docs"
	"apiGateway/router"
	"log"
)

func init() {
	client.RegisterAccountService()
	client.RegisterTaskService()
}

// @title 音转文接口
// @version 1.0
// @description 音转文接口说明
// @host localhost:8090
func main() {
	r := router.InitRouter()
	if err := r.Run(":" + client.Port); err != nil {
		log.Print(err.Error())
	}
}
