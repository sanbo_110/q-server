package forms

type RoleInfoForm struct {
	RoleId     string `json:"roleId" form:"roleId"`
	RoleNum    string `json:"roleNum" form:"roleNum"`
	RoleIdName string `json:"roleIdName" form:"roleIdName"`
	RoleName   string `json:"roleName" form:"roleName"`
	RoleDes    string `json:"roleDes" form:"roleDes"`
	RoleEnable int    `json:"roleEnable" form:"roleEnable"`
}

type RoleInfoListForm struct {
	Page               int64  `form:"page"`
	Limit              int64  `form:"limit"`
	SortField          string `form:"sortField"`
	SortMode           int64  `form:"sortMode"`
	FilterByRoleIdName string `form:"filterByRoleId"`
	FilterCreateAt     int64  `form:"filterCreateAt"`
}
