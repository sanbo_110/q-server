package forms

import . "apiGateway/model/taskService"

type AudioTranslateCreateForm struct {
	AudioName              string `json:"audioName"`
	AudioUri               string `json:"audioUri"`
	AudioTranslateCreateBy string `json:"audioTranslateCreateBy"`
	TranslatePriority      int64  `json:"translatePriority"`
}

type AudioTranslateDeleteForm struct {
	AudioId string `json:"audioId"`
}

type AudioTranslateGetDetailForm struct {
	AudioId string `form:"audioId"`
}

type AudioSubmitSnapShotForm struct {
	AudioId         string                  `json:"audioId"`
	IsRelease       int64                   `json:"isRelease"`
	TranslateResult []TranslateResultEntity `json:"translateResult"`
}

type ReviewSubmitSnapShotForm struct {
	SnapshotId      string                  `json:"snapshotId"`
	TranslateResult []TranslateResultEntity `json:"translateResult"`
}

type AudioWaitingTranslateListForm struct {
	Page                 int64  `form:"page"`
	Limit                int64  `form:"limit"`
	SortField            string `form:"sortField"`
	SortMode             int64  `form:"sortMode"`
	FilterName           string `form:"filterName"`
	FilterCreateAtStart  int64  `form:"filterCreateAtStart"`
	FilterCreateAtEnd    int64  `form:"filterCreateAtEnd"`
	FilterJoinedAtStart  int64  `form:"filterJoinedAtStart"`
	FilterJoinedAtEnd    int64  `form:"filterJoinedAtEnd"`
	FilterTranslateState int64  `form:"FilterTranslateState"`
	FilterDispatchBy     string `form:"filterDispatchBy"`
}

type AudioReviewedSnapshotListForm struct {
	Page             int64  `form:"page"`
	Limit            int64  `form:"limit"`
	SortField        string `form:"sortField"`
	SortMode         int64  `form:"sortMode"`
	FilterName       string `form:"filterName"`
	FilterSnapshotAt int64  `form:"filterSnapshotAt"`
	FilterSnapshotBy string `form:"filterSnapshotBy"`
}

type AudioReviewedSnapshotDetailForm struct {
	SnapshotId string `form:"snapshotId"`
}

type TaskDispatchCreateForm struct {
	AudioId    string `json:"audioId"`
	DispatchTo string `json:"dispatchTo"`
}
