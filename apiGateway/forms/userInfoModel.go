package forms

type UserInfoForm struct {
	UserName         string `json:"userName" form:"userName"`
	UserEmail        string `json:"userEmail" form:"userEmail"`
	UserPhoneNum     string `json:"userPhoneNum" form:"userPhoneNum"`
	UserPassword     string `json:"userPassword"`
	NewUserPassword  string `json:"newUserPassword"`
	SmsCode          int    `json:"smsCode"`
	Token            string `json:"token"`
	UserId           string `json:"userId" form:"userId"`
	LoginMode        string `json:"loginMode"`
	VerifyMode       string `json:"verifyMode"`
	UserIntroduction string `json:"userIntroduction"`
	UserRole         string `json:"userRole" form:"userRole"`
	UserEnable       int    `json:"userEnable"`
	UpdateType       int    `json:"updateType"`
	FilterByUserName string `json:"filterByUserName" form:"filterByUserName"`
	FilterByUserRole string `json:"filterByUserRole" form:"filterByUserRole"`
	Page             int64  `json:"page" form:"page"`
	PageSize         int64  `json:"pageSize" form:"pageSize"`
}
