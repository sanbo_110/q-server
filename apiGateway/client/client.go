package client

import (
	"apiGateway/handler"
	"apiGateway/proto/accountCenter"
	"apiGateway/proto/taskService"
	_ "github.com/asim/go-micro/plugins/registry/kubernetes/v3"
	"github.com/asim/go-micro/v3"
	"github.com/micro/cli/v2"
)

var Port string

const (
	AccountServerName = "Account-Center-Service"
	TaskServerName    = "task-service"
)

func RegisterAccountService() {

	service := micro.NewService(
		micro.Flags(
			&cli.StringFlag{
				Name:  "p",
				Usage: "port",
			},
		),
	)

	service.Init(
		micro.Action(func(c *cli.Context) error {
			Port = c.String("p")
			if len(Port) == 0 {
				Port = "8090"
			}
			return nil
		},
		),
	)

	cli := service.Client()

	handler.AccountServiceClient = accountCenter.NewUserService(AccountServerName, cli)
}

func RegisterTaskService() {

	service := micro.NewService()

	service.Init()

	cli := service.Client()

	handler.TaskServiceClient = taskService.NewAudioService(TaskServerName, cli)
	handler.TaskServiceSnapShotClient = taskService.NewSnapshotService(TaskServerName, cli)
	handler.TaskServiceDispatchClient = taskService.NewReviewService(TaskServerName, cli)
	handler.TaskServiceStatisticClient = taskService.NewStatisticService(TaskServerName, cli)
}
