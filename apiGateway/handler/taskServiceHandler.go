package handler

import (
	. "apiGateway/forms"
	. "apiGateway/model/accountCenter"
	. "apiGateway/model/taskService"
	"apiGateway/proto/accountCenter"
	"apiGateway/proto/taskService"
	. "apiGateway/response"
	"apiGateway/utils"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

var (
	TaskServiceClient          taskService.AudioService
	TaskServiceSnapShotClient  taskService.SnapshotService
	TaskServiceDispatchClient  taskService.ReviewService
	TaskServiceStatisticClient taskService.StatisticService
)

// CreateConvertHandler
// @Description 创建转换任务
// @Id 1
// @Tags 音频转换任务
// @version 1.0
// @Accept application/x-json-stream
// @Param audioName formData string true "音频地址"
// @Param audioUri formData string true "音频名称"
// @Param translatePriority formData int true "转换优先级"
// @Success 200 object AudioTranslateCreateResponseEntity "成功"
// @Failure 400 object AudioTranslateCreateResponseEntity "失败"
// @Router /audio/v1/createConvert [post]
func CreateConvertHandler(c *gin.Context) {

	var translateInfo AudioTranslateCreateForm

	if err := c.ShouldBindJSON(&translateInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, err := TaskServiceClient.Create(context.TODO(), &taskService.CreateRequest{
		AudioName: translateInfo.AudioName,
		AudioUri:  translateInfo.AudioUri,
	})
	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	createResult := AudioTranslateCreateResponseEntity{}
	createResult.Data.AudioId = rsp.AudioId
	createResult.Msg = ""
	createResult.Code = 200
	Success(c, int(createResult.Code), createResult.Msg, createResult.Data)
	return
}

// DeleteConvertHandler
// @Description 删除转换任务
// @Id 15
// @Tags 音频转换任务
// @version 1.0
// @Accept application/x-json-stream
// @Param audioId formData string true "音频ID"
// @Success 200 object AudioTranslateDeleteResponseEntity "成功"
// @Failure 400 object AudioTranslateDeleteResponseEntity "失败"
// @Router /audio/v1/deleteConvert [post]
func DeleteConvertHandler(c *gin.Context) {

	var translateInfo AudioTranslateDeleteForm

	if err := c.ShouldBindJSON(&translateInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	_, err := TaskServiceClient.Delete(context.TODO(), &taskService.DeleteRequest{
		AudioId: translateInfo.AudioId,
	})
	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	deleteResult := AudioTranslateDeleteResponseEntity{}
	deleteResult.Msg = ""
	deleteResult.Code = 200
	Success(c, int(deleteResult.Code), deleteResult.Msg, deleteResult.Data)
	return
}

// GetConvertDetailHandler @Summary
// @Description 查询任务详情
// @Id 2
// @Tags 音频转换任务
// @version 1.0
// @Accept application/x-json-stream
// @Param audioId formData string true "音频ID"
// @Success 200 object TranslateDetailResponseEntity "成功"
// @Failure 400 object TranslateDetailResponseEntity "失败"
// @Router /audio/v1/convertDetail [get]
func GetConvertDetailHandler(c *gin.Context) {

	var detailInfo AudioTranslateGetDetailForm
	if err := c.ShouldBind(&detailInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, err := TaskServiceClient.Detail(context.TODO(), &taskService.DetailRequest{
		AudioId: detailInfo.AudioId,
	})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	hour, minute, seconds := utils.ResolveTime(int(rsp.AudioLength))
	translateDetail := TranslateDetailResponseEntity{}
	translateDetail.Data.AudioId = rsp.AudioId
	translateDetail.Data.AudioName = rsp.AudioName
	translateDetail.Data.AudioUri = rsp.AudioUri
	translateDetail.Data.AudioCreatedAt = rsp.AudioCreatedAt
	translateDetail.Data.TranslateJoinAt = rsp.TranslateJoinAt
	translateDetail.Data.TextCount = rsp.AudioTotalWords
	translateDetail.Data.TextNum = int64(len(rsp.TranslateResult))
	translateDetail.Data.TranslateTaskId = rsp.TranslateTaskId
	translateDetail.Data.AudioLength = rsp.AudioLength
	translateDetail.Data.AudioLengthDes = fmt.Sprintf("%d:%d:%d", hour, minute, seconds)
	translateDetail.Data.TranslateState = rsp.TranslateState
	translateDetail.Data.TranslateCompletedAt = rsp.TranslateCompletedAt
	translateDetail.Code = 200

	var results []*taskService.TranslateResult
	if rsp.SnapshotResult != nil {
		results = rsp.SnapshotResult
	} else {
		results = rsp.TranslateResult
	}

	for _, value := range results {
		hour, minute, seconds := utils.ResolveTime(int(value.StartTime))
		var entity TranslateResultEntity
		entity.Key = value.Key
		entity.Sentences = value.Sentences
		entity.StartTime = value.StartTime
		entity.EndTime = value.EndTime
		entity.Duration = fmt.Sprintf("%d:%d:%d", hour, minute, seconds)
		translateDetail.Data.TranslateResult = append(translateDetail.Data.TranslateResult, entity)
	}

	Success(c, int(translateDetail.Code), translateDetail.Msg, translateDetail.Data)
	return

}

// SubmitConvertSnapShotHandler     @Summary
// @Description 提交暂存
// @Id 13
// @Tags 音频转换任务
// @version 1.0
// @Accept application/x-json-stream
// @Param audioId formData string true "音频ID"
// @Param translateResult body TranslateResultEntity true "暂存数组"
// @Success 200 object AudioUpdateSnapShotResponseEntity "成功"
// @Failure 400 object AudioUpdateSnapShotResponseEntity "失败"
// @Router /audio/v1/convertSubmitSnapShot [post]
func SubmitConvertSnapShotHandler(c *gin.Context) {

	var SnapShotInfo AudioSubmitSnapShotForm
	if err := c.ShouldBind(&SnapShotInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	var rows []*taskService.SnapshotStashRow
	for _, value := range SnapShotInfo.TranslateResult {
		var row taskService.SnapshotStashRow
		row.Sentences = value.Sentences
		row.Key = value.Key
		rows = append(rows, &row)
	}

	rsp, err := TaskServiceSnapShotClient.SnapshotSubmit(context.TODO(), &taskService.SnapshotSubmitRequest{
		AudioId:    SnapShotInfo.AudioId,
		IsRelease:  &SnapShotInfo.IsRelease,
		UpdateRows: rows,
	})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	snapShotInfo := AudioUpdateSnapShotResponseEntity{}
	snapShotInfo.Code = 200
	snapShotInfo.Data.SnapShotId = rsp.SnapshotId
	Success(c, int(snapShotInfo.Code), snapShotInfo.Msg, snapShotInfo.Data)
	return
}

// SubmitReviewSnapShotHandler      @Summary
// @Description 快照回滚&&快照二次更新
// @Id 33
// @Tags 审核任务
// @version 1.0
// @Accept application/x-json-stream
// @Param audioId formData string true "音频ID"
// @Param translateResult body TranslateResultEntity true "二次更新需要传更新数组，不传数组就是快照回滚"
// @Success 200 object AudioUpdateSnapShotResponseEntity "成功"
// @Failure 400 object AudioUpdateSnapShotResponseEntity "失败"
// @Router /review/v1/SubmitSnapShot [post]
func SubmitReviewSnapShotHandler(c *gin.Context) {

	var SnapShotInfo ReviewSubmitSnapShotForm
	if err := c.ShouldBind(&SnapShotInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	var rows []*taskService.SnapshotStashRow
	for _, value := range SnapShotInfo.TranslateResult {
		var row taskService.SnapshotStashRow
		row.Sentences = value.Sentences
		row.Key = value.Key
		rows = append(rows, &row)
	}

	rsp, err := TaskServiceSnapShotClient.SnapshotCover(context.TODO(), &taskService.SnapshotCoverRequest{
		SnapshotId: SnapShotInfo.SnapshotId,
		UpdateRows: rows,
	})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	snapShotInfo := AudioUpdateSnapShotResponseEntity{}
	snapShotInfo.Code = 200
	snapShotInfo.Data.SnapShotId = rsp.SnapshotId
	Success(c, int(snapShotInfo.Code), snapShotInfo.Msg, snapShotInfo.Data)
	return

}

// GetConvertListHandler  @Summary
// @Description 待转换任务列表
// @Id 14
// @Tags 音频转换任务
// @version 1.0
// @Accept application/x-json-stream
// @Param page formData int false "页数"
// @Param limit formData int false "单页返回个数"
// @Param sortField formData string false "排序关键字"
// @Param sortMode formData  int false " 排序模式"
// @Param filterName formData  string false "按名称过滤的关键字"
// @Param filterCreateAt formData  int false "按创建时间过滤的关键字"
// @Param filterJoinedAt formData  int false "按加入时间过滤的关键字"
// @Param filterTranslateState formData  int false "按状态过滤的关键字"
// @Success 200 object AudioCovertListResponseEntity "成功"
// @Failure 400 object AudioCovertListResponseEntity "失败"
// @Router /audio/v1/convertList [get]
func GetConvertListHandler(c *gin.Context) {

	var getListForm AudioWaitingTranslateListForm

	if err := c.ShouldBind(&getListForm); err != nil {
		log.Println("err ->", err.Error())
	}
	var taskQuery taskService.QueryRequest
	taskQuery = taskService.QueryRequest{}

	taskQuery.Page = &getListForm.Page
	taskQuery.Limit = &getListForm.Limit
	taskQuery.SortField = &getListForm.SortField
	taskQuery.SortMode = &getListForm.SortMode
	taskQuery.FilterName = &getListForm.FilterName
	taskQuery.FilterTranslateState = &getListForm.FilterTranslateState
	taskQuery.FilterDispatchBy = nil

	if getListForm.FilterCreateAtStart != 0 && getListForm.FilterCreateAtEnd != 0 {
		taskQuery.FilterJoinAt = &taskService.TimestampRange{Start: getListForm.FilterJoinedAtStart, End: getListForm.FilterJoinedAtEnd}
	}

	if getListForm.FilterJoinedAtStart != 0 && getListForm.FilterJoinedAtEnd != 0 {
		taskQuery.FilterCreatedAt = &taskService.TimestampRange{Start: getListForm.FilterCreateAtStart, End: getListForm.FilterCreateAtEnd}
	}

	rsp, err := TaskServiceClient.Query(context.TODO(), &taskQuery)

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	audioList := AudioCovertListResponseEntity{}
	audioList.Data.Limit = rsp.Limit
	audioList.Data.Page = rsp.Page
	audioList.Code = 200
	timeLayout := "2006-01-02 15:04:05"
	for _, value := range rsp.Rows {
		if value.TranslateState == int64(taskService.State_TranslateWaiting) || value.TranslateState == int64(taskService.State_TranslateJoined) || value.TranslateState == int64(taskService.State_TranslateFailed) {
			hour, minute, seconds := utils.ResolveTime(int(value.AudioLength))
			var entity CovertListRowDetailEntity
			entity.AudioId = value.AudioId
			entity.AudioName = value.AudioName
			entity.AudioLength = fmt.Sprintf("%d:%d:%d", hour, minute, seconds)
			entity.AudioCreatedAt = time.Unix(value.AudioCreatedAt, 0).Format(timeLayout)

			entity.AudioCreatedBy = value.AudioCreatedBy
			entity.AudioState = value.AudioState
			if value.TranslateJoinAt != 0 {
				entity.TranslateJoinAt = time.Unix(value.TranslateJoinAt, 0).Format(timeLayout)
			}
			entity.TranslateJoinBy = value.TranslateJoinBy
			entity.TranslateCompletedAt = time.Unix(value.TranslateCompletedAt, 0).Format(timeLayout)
			entity.TranslateState = value.TranslateState
			if value.AudioState == 2 {
				entity.TranslateState = 5
			}
			entity.AudioTotalWords = value.AudioTotalWords
			audioList.Data.Rows = append(audioList.Data.Rows, entity)
		}
	}
	audioList.Data.Total = int64(len(audioList.Data.Rows))
	Success(c, int(audioList.Code), audioList.Msg, audioList.Data)
	return

}

// GetConvertSnapshotListHandler   @Summary
// @Description 已审核快照列表
// @Id 16
// @Tags 音频转换任务
// @version 1.0
// @Accept application/x-json-stream
// @Param page formData int false "页数"
// @Param limit formData int false "单页返回个数"
// @Param sortField formData string false "排序关键字"
// @Param sortMode formData  int false " 排序模式"
// @Param filterName formData  string false "按名称过滤的关键字"
// @Param filterSnapshotAt formData  int false "按创建时间过滤的关键字"
// @Param filterSnapshotBy formData  string false "按加创建人过滤的关键字"
// @Success 200 object SnapshotListResponseEntity "成功"
// @Failure 400 object SnapshotListResponseEntity "失败"
// @Router /audio/v1/convertSnapshotList [get]
func GetConvertSnapshotListHandler(c *gin.Context) {

	var getListForm AudioReviewedSnapshotListForm

	if err := c.ShouldBind(&getListForm); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, err := TaskServiceSnapShotClient.SnapshotQuery(context.TODO(), &taskService.SnapshotQueryRequest{
		Page:             &getListForm.Page,
		Limit:            &getListForm.Limit,
		SortField:        &getListForm.SortField,
		SortMode:         &getListForm.SortMode,
		FilterName:       &getListForm.FilterName,
		FilterSnapshotBy: &getListForm.FilterSnapshotBy,
	})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	snapshotList := SnapshotListResponseEntity{}
	snapshotList.Data.Total = rsp.Total
	snapshotList.Data.Limit = rsp.Limit
	snapshotList.Data.Page = rsp.Page
	snapshotList.Code = 200
	timeLayout := "2006-01-02 15:04:05"
	for _, value := range rsp.Rows {
		var entity SnapshotRowDetailEntity
		entity.SnapshotId = value.SnapshotId
		entity.SnapshotAt = time.Unix(value.SnapshotAt, 0).Format(timeLayout)
		entity.SnapshotBy = value.SnapshotBy
		entity.SnapshotDiffRows = value.SnapshotDiffRows
		entity.SnapshotIsRelease = value.SnapshotIsRelease
		entity.AudioId = value.AudioId
		entity.AudioName = value.AudioName
		snapshotList.Data.Rows = append(snapshotList.Data.Rows, entity)
	}
	Success(c, int(snapshotList.Code), snapshotList.Msg, snapshotList.Data)
	return

}

// GetConvertSnapshotDetailHandler       @Summary
// @Description 审核快照详情
// @Id 34
// @Tags 音频转换任务
// @version 1.0
// @Accept application/x-json-stream
// @Param snapshotId formData string true "快照ID"
// @Success 200 object SnapshotDetailResponseEntity "成功"
// @Failure 400 object SnapshotDetailResponseEntity "失败"
// @Router /audio/v1/convertSnapshotDetail [post]
func GetConvertSnapshotDetailHandler(c *gin.Context) {

	var snapshotDetail AudioReviewedSnapshotDetailForm

	if err := c.ShouldBind(&snapshotDetail); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, err := TaskServiceSnapShotClient.SnapshotDetail(context.TODO(), &taskService.SnapshotDetailRequest{
		SnapshotId: snapshotDetail.SnapshotId,
	})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	hour, minute, seconds := utils.ResolveTime(int(rsp.AudioLength))
	timeLayout := "2006-01-02 15:04:05"

	snapshotDetailResponse := SnapshotDetailResponseEntity{}
	snapshotDetailResponse.Code = 200

	snapshotDetailResponse.Data.SnapshotId = rsp.SnapshotId
	snapshotDetailResponse.Data.SnapshotAt = time.Unix(rsp.SnapshotAt, 0).Format(timeLayout)
	snapshotDetailResponse.Data.SnapshotBy = rsp.SnapshotBy
	snapshotDetailResponse.Data.SnapshotDiffRows = rsp.SnapshotDiffRows
	snapshotDetailResponse.Data.SnapshotIsRelease = rsp.SnapshotIsRelease
	snapshotDetailResponse.Data.AudioId = rsp.AudioId
	snapshotDetailResponse.Data.AudioName = rsp.AudioName
	snapshotDetailResponse.Data.AudioLength = fmt.Sprintf("%d:%d:%d", hour, minute, seconds)
	snapshotDetailResponse.Data.AudioCreatedAt = time.Unix(rsp.AudioCreatedAt, 0).Format(timeLayout)
	snapshotDetailResponse.Data.AudioCreatedBy = rsp.AudioCreatedBy
	snapshotDetailResponse.Data.AudioState = rsp.AudioState
	snapshotDetailResponse.Data.AudioTotalWords = rsp.AudioTotalWords
	for _, value := range rsp.SnapshotDiffResult {
		var entity SnapshotDiffResultEntity
		hour, minute, seconds := utils.ResolveTime(int(value.StartTime))
		entity.Key = value.Key
		entity.OldSentences = value.OldSentences
		entity.NewSentences = value.NewSentences
		entity.Duration = fmt.Sprintf("%d:%d:%d", hour, minute, seconds)
		snapshotDetailResponse.Data.SnapshotDiffResult = append(snapshotDetailResponse.Data.SnapshotDiffResult, entity)
	}
	Success(c, int(snapshotDetailResponse.Code), snapshotDetailResponse.Msg, snapshotDetailResponse.Data)
}

// DispatchTaskHandler
// @Description 创建手工派单
// @Id 17
// @Tags 派单任务
// @version 1.0
// @Accept application/x-json-stream
// @Param audioId formData string true "被指派音频 ID"
// @Param dispatchTo formData string true "被指派人用户 ID"
// @Success 200 object DispatchCreateResponseEntity "成功"
// @Failure 400 object DispatchCreateResponseEntity "失败"
// @Router /dispatch/v1/addTask [post]
func DispatchTaskHandler(c *gin.Context) {

	var dispatchInfo TaskDispatchCreateForm

	if err := c.ShouldBindJSON(&dispatchInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	_, err := TaskServiceDispatchClient.DispatchAppoint(context.TODO(), &taskService.DispatchAppointRequest{
		AudioId:    dispatchInfo.AudioId,
		DispatchBy: dispatchInfo.DispatchTo,
	})
	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	createResult := DispatchCreateResponseEntity{}
	createResult.Msg = ""
	createResult.Code = 200
	Success(c, int(createResult.Code), createResult.Msg, createResult.Data)
	return
}

// WaitDispatchListHandler
// @Description 待派单列表
// @Id 18
// @Tags 派单任务
// @version 1.0
// @Accept application/x-json-stream
// @Param audioId formData string true "被指派音频 ID"
// @Param dispatchTo formData string true "被指派人用户 ID"
// @Success 200 object DispatchCreateResponseEntity "成功"
// @Failure 400 object DispatchCreateResponseEntity "失败"
// @Router /dispatch/v1/waitList [post]
func WaitDispatchListHandler(c *gin.Context) {

	var getListForm AudioWaitingTranslateListForm

	if err := c.ShouldBind(&getListForm); err != nil {
		log.Println("err ->", err.Error())
	}

	var taskQuery taskService.QueryRequest
	taskQuery = taskService.QueryRequest{}

	taskQuery.Page = &getListForm.Page
	taskQuery.Limit = &getListForm.Limit
	taskQuery.SortField = &getListForm.SortField
	taskQuery.SortMode = &getListForm.SortMode
	taskQuery.FilterName = &getListForm.FilterName
	taskQuery.FilterTranslateState = &getListForm.FilterTranslateState

	if getListForm.FilterDispatchBy != "" {
		taskQuery.FilterDispatchBy = &getListForm.FilterDispatchBy
	}

	if getListForm.FilterCreateAtStart != 0 && getListForm.FilterCreateAtEnd != 0 {
		taskQuery.FilterJoinAt = &taskService.TimestampRange{Start: getListForm.FilterJoinedAtStart, End: getListForm.FilterJoinedAtEnd}
	}

	if getListForm.FilterJoinedAtStart != 0 && getListForm.FilterJoinedAtEnd != 0 {
		taskQuery.FilterCreatedAt = &taskService.TimestampRange{Start: getListForm.FilterCreateAtStart, End: getListForm.FilterCreateAtEnd}
	}

	rsp, err := TaskServiceClient.Query(context.TODO(), &taskQuery)

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	audioList := AudioCovertListResponseEntity{}
	audioList.Data.Limit = rsp.Limit
	audioList.Data.Page = rsp.Page
	audioList.Code = 200
	timeLayout := "2006-01-02 15:04:05"
	for _, value := range rsp.Rows {
		if value.TranslateState == int64(taskService.State_TranslateCompleted) {

			hour, minute, seconds := utils.ResolveTime(int(value.AudioLength))
			var entity CovertListRowDetailEntity

			if value.DispatchBy != "" {
				rsp, _ := AccountServiceClient.QueryUserDetail(context.TODO(), &accountCenter.QueryUserDetailRequest{
					UserId: value.DispatchBy,
				})
				entity.DispatchBy = rsp.UserInfo.UserName
			}

			entity.AudioId = value.AudioId
			entity.AudioName = value.AudioName
			entity.AudioLength = fmt.Sprintf("%d:%d:%d", hour, minute, seconds)
			entity.AudioCreatedAt = time.Unix(value.AudioCreatedAt, 0).Format(timeLayout)
			entity.AudioCreatedBy = value.AudioCreatedBy
			entity.AudioState = value.AudioState
			if value.DispatchAt != 0 {
				entity.DispatchAt = time.Unix(value.DispatchAt, 0).Format(timeLayout)
			}
			if value.ReleaseAt != 0 {
				entity.ReleaseAt = time.Unix(value.ReleaseAt, 0).Format(timeLayout)
			}
			if value.ReviewAt != 0 {
				entity.ReviewAt = time.Unix(value.ReviewAt, 0).Format(timeLayout)
			}
			if value.TranslateJoinAt != 0 {
				entity.TranslateJoinAt = time.Unix(value.TranslateJoinAt, 0).Format(timeLayout)
			}
			entity.TranslateJoinBy = value.TranslateJoinBy
			if value.TranslateCompletedAt != 0 {
				entity.TranslateCompletedAt = time.Unix(value.TranslateCompletedAt, 0).Format(timeLayout)
			}
			entity.ReleaseState = value.ReleaseState
			entity.TranslateState = value.TranslateState
			entity.AudioTotalWords = value.AudioTotalWords

			if value.ReleaseState {
				entity.ReviewState = 1

				if value.ReleaseAt != 0 {
					entity.ReviewTime = time.Unix(value.ReleaseAt, 0).Format(timeLayout)
				}

			} else {

				if value.ReleaseAt == 0 {

					if value.ReviewAt == 0 {
						entity.ReviewState = 4
						if value.DispatchAt != 0 {
							entity.ReviewTime = time.Unix(value.DispatchAt, 0).Format(timeLayout)
						}

					} else {
						entity.ReviewState = 3
						if value.ReviewAt != 0 {
							entity.ReviewTime = time.Unix(value.ReviewAt, 0).Format(timeLayout)
						}
					}

				} else {

					entity.ReviewState = 2
					if value.ReleaseAt != 0 {
						entity.ReviewTime = time.Unix(value.ReleaseAt, 0).Format(timeLayout)
					}
				}
			}

			audioList.Data.Rows = append(audioList.Data.Rows, entity)
		}
	}
	audioList.Data.Total = int64(len(audioList.Data.Rows))
	Success(c, int(audioList.Code), audioList.Msg, audioList.Data)
	return
}

// GetUnReviewListHandler   @Summary
// @Description 待审核列表
// @Id 19
// @Tags 审核任务
// @version 1.0
// @Accept application/x-json-stream
// @Param page formData int false "页数"
// @Param limit formData int false "单页返回个数"
// @Param sortField formData string false "排序关键字"
// @Param sortMode formData  int false " 排序模式"
// @Param filterName formData  string false "按名称过滤的关键字"
// @Param filterCreateAt formData  int false "按创建时间过滤的关键字"
// @Param filterJoinedAt formData  int false "按加入时间过滤的关键字"
// @Param filterTranslateState formData  int false "按状态过滤的关键字"
// @Success 200 object AudioCovertListResponseEntity "成功"
// @Failure 400 object AudioCovertListResponseEntity "失败"
// @Router /review/v1/waitReview [get]
func GetUnReviewListHandler(c *gin.Context) {

	var getListForm AudioWaitingTranslateListForm

	if err := c.ShouldBind(&getListForm); err != nil {
		log.Println("err ->", err.Error())
	}

	var taskQuery taskService.QueryRequest
	taskQuery = taskService.QueryRequest{}

	taskQuery.Page = &getListForm.Page
	taskQuery.Limit = &getListForm.Limit
	taskQuery.SortField = &getListForm.SortField
	taskQuery.SortMode = &getListForm.SortMode
	taskQuery.FilterName = &getListForm.FilterName
	taskQuery.FilterTranslateState = &getListForm.FilterTranslateState

	if getListForm.FilterDispatchBy != "" {
		taskQuery.FilterDispatchBy = &getListForm.FilterDispatchBy
	}

	if getListForm.FilterCreateAtStart != 0 && getListForm.FilterCreateAtEnd != 0 {
		taskQuery.FilterJoinAt = &taskService.TimestampRange{Start: getListForm.FilterJoinedAtStart, End: getListForm.FilterJoinedAtEnd}
	}

	if getListForm.FilterJoinedAtStart != 0 && getListForm.FilterJoinedAtEnd != 0 {
		taskQuery.FilterCreatedAt = &taskService.TimestampRange{Start: getListForm.FilterCreateAtStart, End: getListForm.FilterCreateAtEnd}
	}

	rsp, err := TaskServiceClient.Query(context.TODO(), &taskQuery)

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	audioList := AudioCovertListResponseEntity{}
	audioList.Data.Total = rsp.Total
	audioList.Data.Limit = rsp.Limit
	audioList.Data.Page = rsp.Page
	audioList.Code = 200
	timeLayout := "2006-01-02 15:04:05"
	for _, value := range rsp.Rows {
		if value.TranslateState == int64(taskService.State_TranslateCompleted) {
			hour, minute, seconds := utils.ResolveTime(int(value.AudioLength))
			var entity CovertListRowDetailEntity
			entity.AudioId = value.AudioId
			entity.AudioName = value.AudioName
			entity.AudioLength = fmt.Sprintf("%d:%d:%d", hour, minute, seconds)
			entity.AudioCreatedAt = time.Unix(value.AudioCreatedAt, 0).Format(timeLayout)
			entity.AudioCreatedBy = value.AudioCreatedBy
			entity.AudioState = value.AudioState
			entity.TranslateJoinBy = value.TranslateJoinBy

			if value.TranslateCompletedAt != 0 {
				entity.TranslateCompletedAt = time.Unix(value.TranslateCompletedAt, 0).Format(timeLayout)
			}

			if value.DispatchAt != 0 {
				entity.DispatchAt = time.Unix(value.DispatchAt, 0).Format(timeLayout)
			}

			if value.ReleaseAt != 0 {
				entity.ReleaseAt = time.Unix(value.ReleaseAt, 0).Format(timeLayout)
			}
			if value.TranslateJoinAt != 0 {
				entity.TranslateJoinAt = time.Unix(value.TranslateJoinAt, 0).Format(timeLayout)
			}
			entity.ReleaseState = value.ReleaseState
			entity.TranslateState = value.TranslateState
			entity.AudioTotalWords = value.AudioTotalWords
			audioList.Data.Rows = append(audioList.Data.Rows, entity)
		}
	}
	audioList.Data.Total = int64(len(audioList.Data.Rows))
	Success(c, int(audioList.Code), audioList.Msg, audioList.Data)
	return

}

func GetTotalTranslateLengthHandler(c *gin.Context) {

	var getListForm AudioWaitingTranslateListForm

	if err := c.ShouldBind(&getListForm); err != nil {
		log.Println("err ->", err.Error())
	}
	rsp, _ := TaskServiceStatisticClient.PlatformTotalTranslateAudioLength(context.TODO(), &taskService.StatisticEmptyRequest{})
	hour, minute, seconds := utils.ResolveTime(int(rsp.Value))
	test := BaseResponseEntity{}
	test.Data = fmt.Sprintf("%d:%d:%d", hour, minute, seconds)
	test.Code = 200
	Success(c, int(test.Code), test.Msg, test.Data)
}
