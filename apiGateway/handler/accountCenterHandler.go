package handler

import (
	. "apiGateway/forms"
	. "apiGateway/model/accountCenter"
	"apiGateway/proto/accountCenter"
	. "apiGateway/response"
	"apiGateway/utils"
	"context"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
	"time"
)

var (
	AccountServiceClient accountCenter.UserService
)

// UserRegisterHandler   @Summary
// @Description 注册用户
// @Id 5
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param userPhoneNum formData string true "用户手机号"
// @Param userEmail formData string true "用户邮箱"
// @Param userPassword formData string true "用户密码"
// @Param smsCode formData string true "验证码"
// @Success 200 object RegisterResponseEntity "注册成功"
// @Failure 3000 object RegisterResponseEntity "注册失败"
// @Router /auth/v1/register [post]
func UserRegisterHandler(c *gin.Context) {

	var userInfo UserInfoForm

	if err := c.ShouldBindJSON(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, err := AccountServiceClient.CreatUser(context.TODO(), &accountCenter.CreateUserRequest{
		PhoneNum: userInfo.UserPhoneNum,
		Email:    userInfo.UserEmail,
		Password: userInfo.UserPassword,
		SmsCode:  strconv.FormatInt(int64(userInfo.SmsCode), 10),
	})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	register := RegisterResponseEntity{
		Code: rsp.Code,
		Msg:  rsp.Msg,
		Data: UserInfoResponseEntity{
			UserId:   rsp.UserId,
			UserName: rsp.UserName,
		},
	}

	Success(c, int(register.Code), register.Msg, register.Data)
}

// UserLoginHandler   @Summary
// @Description 用户登录
// @Id 4
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param userPhoneNum formData string false "用户手机号"
// @Param userEmail formData string false "用户邮箱"
// @Param userPassword formData string false "用户密码"
// @Param smsCode formData string false "验证码"
// @Param loginMode formData string true "登录方式 sms短信登录、password密码登录"
// @Success 200 object LoginResponseEntity "登录成功"
// @Failure 4000 object LoginResponseEntity "登录失败"
// @Router /auth/v1/login [post]
func UserLoginHandler(c *gin.Context) {

	var userInfo UserInfoForm

	if err := c.ShouldBindJSON(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	var err error
	var rsp *accountCenter.LoginResponse

	if userInfo.LoginMode == "sms" {
		rsp, err = AccountServiceClient.LoginWithSms(context.TODO(), &accountCenter.LoginRequest{
			PhoneNum: userInfo.UserPhoneNum,
			SmsCode:  int64(userInfo.SmsCode),
		})
	} else if userInfo.LoginMode == "pwd" {
		rsp, err = AccountServiceClient.Login(context.TODO(), &accountCenter.LoginRequest{
			PhoneNum: userInfo.UserPhoneNum,
			Email:    userInfo.UserEmail,
			Password: userInfo.UserPassword,
		})
	}

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	if rsp.Success {

		userInfo.UserId = rsp.UserId

		token, err := utils.GenerateToken(c, userInfo)

		if err != nil {
			Success(c, -1, err.Error(), nil)
			return
		}

		_, updateErr := AccountServiceClient.UpdateUser(context.TODO(), &accountCenter.UpdateUserRequest{
			UserId: rsp.UserId,
			Token:  token,
		})

		if updateErr != nil {
			Success(c, -1, updateErr.Error(), nil)
			return
		}

		user := UserInfoResponseEntity{
			Token:      token,
			UserId:     rsp.UserId,
			Permission: rsp.Permission,
		}

		loginResponse := LoginResponseEntity{
			Code: rsp.Code,
			Msg:  rsp.Msg,
			Data: user,
		}
		Success(c, int(loginResponse.Code), loginResponse.Msg, loginResponse.Data)
		return

	} else {
		loginResponse := LoginResponseEntity{
			Code: rsp.Code,
			Msg:  rsp.Msg,
		}
		Success(c, int(loginResponse.Code), loginResponse.Msg, loginResponse.Data)
		return
	}

}

// VerifyUserInfoHandler    @Summary
// @Description 用户身份验证
// @Id 20
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param userPhoneNum formData string false "用户手机号"
// @Param userEmail formData string false "用户邮箱"
// @Param smsCode formData string false "验证码"
// @Success 200 object BaseResponseEntity "验证成功"
// @Failure 4000 object BaseResponseEntity "验证失败"
// @Router /auth/v1/verifyUserInfo [post]
func VerifyUserInfoHandler(c *gin.Context) {
	var userInfo UserInfoForm

	if err := c.ShouldBind(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}
	rsp, error := AccountServiceClient.QueryUserDetail(context.TODO(), &accountCenter.QueryUserDetailRequest{
		UserPhoneNum: userInfo.UserPhoneNum,
		UserEmail:    userInfo.UserEmail,
	})

	if error != nil {
		log.Print(error.Error())
		Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
		return
	}

	if rsp.Success {

		vrsp, verror := AccountServiceClient.VerifyUserSendCode(context.TODO(), &accountCenter.VerifyCodeRequest{
			PhoneNum: userInfo.UserPhoneNum,
			Email:    userInfo.UserEmail,
			VCode:    strconv.FormatInt(int64(userInfo.SmsCode), 10),
		})

		if verror != nil {
			log.Print(error.Error())
			Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
			return
		}

		Success(c, int(vrsp.Code), vrsp.Msg, nil)
		return

	} else {
		Success(c, int(rsp.Code), rsp.Msg, nil)
		return
	}

	return

}

// ResetUserPwdHandler     @Summary
// @Description 重置密码
// @Id 21
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param userPhoneNum formData string false "用户手机号"
// @Param userEmail formData string false "用户邮箱"
// @Param newUserPassword formData string false "用户密码"
// @Success 200 object BaseResponseEntity "修改成功"
// @Failure 4000 object BaseResponseEntity "修改失败"
// @Router /auth/v1/resetPassWord [post]
func ResetUserPwdHandler(c *gin.Context) {
	var userInfo UserInfoForm

	if err := c.ShouldBind(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, _ := AccountServiceClient.QueryUserDetail(context.TODO(), &accountCenter.QueryUserDetailRequest{
		UserPhoneNum: userInfo.UserPhoneNum,
		UserEmail:    userInfo.UserEmail,
	})

	if !rsp.Success {
		Success(c, int(rsp.Code), rsp.Msg, nil)
		return
	}

	updateRsp, error := AccountServiceClient.UpdateUser(context.TODO(), &accountCenter.UpdateUserRequest{
		UserId:          rsp.UserInfo.UserId,
		NewUserPassword: userInfo.NewUserPassword,
		UpdateType:      int64(accountCenter.ModifyType_UserResetPwd),
	})

	if error != nil {
		log.Print(error.Error())
		Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
		return
	}
	Success(c, int(updateRsp.Code), updateRsp.Msg, nil)
	return
}

// UpdateUserInfoHandler      @Summary
// @Description 修改用户信息
// @Id 22
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param userId formData string true "用户ID"
// @Param userPhoneNum formData string false "用户手机号"
// @Param userEmail formData string false "用户邮箱"
// @Param userName formData string false "用户名称"
// @Param newUserPassword formData string false "用户密码"
// @Param userIntroduction formData string false "用户简介"
// @Param userRole formData string false "用户角色"
// @Param userEnable formData int false "用户是否可用状态 0不可以用 1可用"
// @Success 200 object QueryUserInfoResponseEntity "修改成功"
// @Failure 4000 object QueryUserInfoResponseEntity "修改失败"
// @Router /user/v1/updateUserInfo [post]
func UpdateUserInfoHandler(c *gin.Context) {

	var userInfo UserInfoForm

	if err := c.ShouldBind(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}
	updateRsp, error := AccountServiceClient.UpdateUser(context.TODO(), &accountCenter.UpdateUserRequest{
		UserId:           userInfo.UserId,
		UserName:         userInfo.UserName,
		UserEmail:        userInfo.UserEmail,
		UserPhoneNum:     userInfo.UserPhoneNum,
		UserRole:         userInfo.UserRole,
		UserIntroduction: userInfo.UserIntroduction,
		UserEnable:       int64(userInfo.UserEnable),
		UserVCode:        strconv.FormatInt(int64(userInfo.SmsCode), 10),
		UserPassword:     userInfo.UserPassword,
		NewUserPassword:  userInfo.NewUserPassword,
		UpdateType:       int64(userInfo.UpdateType),
	})

	if error != nil {
		log.Print(error.Error())
		Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
		return
	}

	if updateRsp.Success {

		rsp, queryError := AccountServiceClient.QueryUserDetail(context.TODO(), &accountCenter.QueryUserDetailRequest{
			UserId: userInfo.UserId,
		})

		if queryError != nil {
			log.Print(queryError.Error())
			Err(c, -1, http.StatusInternalServerError, queryError.Error(), nil)
			return
		}

		userInfoResponse := QueryUserInfoResponseEntity{
			Code: rsp.Code,
			Msg:  rsp.Msg,
		}

		userInfoResponse.Data.UserName = rsp.UserInfo.UserName
		userInfoResponse.Data.UserEmail = rsp.UserInfo.UserEmail
		userInfoResponse.Data.UserPhoneNum = rsp.UserInfo.UserPhoneNum
		userInfoResponse.Data.UserPassword = rsp.UserInfo.UserPassword
		userInfoResponse.Data.Permission = rsp.UserInfo.Permission
		userInfoResponse.Data.UserIntroduction = rsp.UserInfo.UserIntroduction
		userInfoResponse.Data.UserRole = rsp.UserInfo.UserRole
		userInfoResponse.Data.UserAvatar = rsp.UserInfo.UserAvatar
		userInfoResponse.Data.UserEnable = int(rsp.UserInfo.UserEnable)
		Success(c, int(userInfoResponse.Code), userInfoResponse.Msg, userInfoResponse.Data)

	} else {
		Success(c, int(updateRsp.Code), updateRsp.Msg, nil)
	}
	return

}

// UserInfoHandler       @Summary
// @Description 获取用户信息
// @Id 23
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param userId formData string false "用户Id"
// @Param userPhoneNum formData string false "用户手机号"
// @Param userEmail formData string false "用户邮箱"
// @Success 200 object QueryUserInfoResponseEntity "成功"
// @Failure 4000 object QueryUserInfoResponseEntity "失败"
// @Router /user/v1/updateUserInfo [get]
func UserInfoHandler(c *gin.Context) {

	var userInfo UserInfoForm

	if err := c.ShouldBind(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}
	rsp, error := AccountServiceClient.QueryUserDetail(context.TODO(), &accountCenter.QueryUserDetailRequest{
		UserId:       userInfo.UserId,
		UserPhoneNum: userInfo.UserPhoneNum,
		UserEmail:    userInfo.UserEmail,
	})

	if error != nil {
		log.Print(error.Error())
		Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
		return
	}
	if rsp.Success {
		userInfoResponse := QueryUserInfoResponseEntity{
			Code: rsp.Code,
			Msg:  rsp.Msg,
		}

		userInfoResponse.Data.UserName = rsp.UserInfo.UserName
		userInfoResponse.Data.UserEmail = rsp.UserInfo.UserEmail
		userInfoResponse.Data.UserPhoneNum = rsp.UserInfo.UserPhoneNum
		userInfoResponse.Data.UserPassword = rsp.UserInfo.UserPassword
		userInfoResponse.Data.Permission = rsp.UserInfo.Permission
		userInfoResponse.Data.UserIntroduction = rsp.UserInfo.UserIntroduction
		userInfoResponse.Data.UserRole = rsp.UserInfo.UserRole
		userInfoResponse.Data.UserAvatar = rsp.UserInfo.UserAvatar
		userInfoResponse.Data.UserEnable = int(rsp.UserInfo.UserEnable)
		userInfoResponse.Data.LoginTime = rsp.UserInfo.LoginTime
		Success(c, int(userInfoResponse.Code), userInfoResponse.Msg, userInfoResponse.Data)
		return
	} else {
		Success(c, int(rsp.Code), rsp.Msg, nil)
		return
	}

}

func UserNavHandler(c *gin.Context) {
	var userInfo UserInfoForm

	if err := c.ShouldBind(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, error := AccountServiceClient.QueryUserDetail(context.TODO(), &accountCenter.QueryUserDetailRequest{
		UserId: userInfo.UserId,
	})

	if error != nil {
		log.Print(error.Error())
		Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
		return
	}
	var jsonResponse string
	if rsp.UserInfo.Permission == "staff" {
		jsonResponse = StaffNav
	} else if rsp.UserInfo.Permission == "super" {
		jsonResponse = SuperNav
	} else if rsp.UserInfo.Permission == "admin" {
		jsonResponse = AdminNav
	} else {
		jsonResponse = StaffNav
	}

	jsonResponse = SuperNav

	c.String(http.StatusOK, jsonResponse)

	//userNavResponse := QueryUserNavResponseEntity{}
	//userNavResponse.Code = 200
	//Success(c, int(userNavResponse.Code), userNavResponse.Msg, jsonResponse)
}

// UserSmsCodeHandler @Summary
// @Description 获取短信验证码
// @Id 35
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param userPhoneNum formData string false "用户手机号"
// @Success 200 object VerificationCodeResponseEntity "成功"
// @Failure 4000 object VerificationCodeResponseEntity "失败"
// @Router /auth/v1/smsCode [get]
func UserSmsCodeHandler(c *gin.Context) {

	var userInfo UserInfoForm
	if err := c.ShouldBind(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}
	rsp, error := AccountServiceClient.UserSendCode(context.TODO(), &accountCenter.SendCodeRequest{
		PhoneNum: userInfo.UserPhoneNum,
		Type:     "sms",
	})
	if error != nil {
		log.Print(error.Error())
		Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
		return
	}

	codeResponse := VerificationCodeResponseEntity{
		Code: 200,
		Msg:  rsp.Msg,
	}
	codeResponse.Data.VCode = rsp.CodeValue
	Success(c, int(codeResponse.Code), codeResponse.Msg, codeResponse.Data)
	return
}

// UserEmailCodeHandler  @Summary
// @Description 获取邮箱验证码
// @Id 36
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param userEmail formData string false "用户邮箱"
// @Success 200 object VerificationCodeResponseEntity "成功"
// @Failure 4000 object VerificationCodeResponseEntity "失败"
// @Router /auth/v1/emailCode [get]
func UserEmailCodeHandler(c *gin.Context) {

	var userInfo UserInfoForm
	if err := c.ShouldBind(&userInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, error := AccountServiceClient.UserSendCode(context.TODO(), &accountCenter.SendCodeRequest{
		Email: userInfo.UserEmail,
		Type:  "email",
	})
	if error != nil {
		log.Print(error.Error())
		Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
		return
	}
	codeResponse := VerificationCodeResponseEntity{
		Code: 200,
		Msg:  rsp.Msg,
	}
	codeResponse.Data.VCode = rsp.CodeValue
	Success(c, int(codeResponse.Code), codeResponse.Msg, codeResponse.Data)

}

// UserListHandler   @Summary
// @Description 用户列表
// @Id 37
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param page formData int false "页数"
// @Param pageSize formData int false "单页返回个数"
// @Param filterByUserName formData string false "按用户名称过滤"
// @Param filterByUserRole formData  string false "按用户角色过滤"
// @Success 200 object UserListResponseEntity "成功"
// @Failure 400 object UserListResponseEntity "失败"
// @Router /user/v1/userList [get]
func UserListHandler(c *gin.Context) {

	var getListForm UserInfoForm

	if err := c.ShouldBind(&getListForm); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, err := AccountServiceClient.QueryUsers(context.TODO(), &accountCenter.QueryUsersRequest{
		FilterUserRole: &getListForm.FilterByUserRole,
		FilterUserName: &getListForm.FilterByUserName,
		Page:           &getListForm.Page,
		Limit:          &getListForm.PageSize,
	})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	userList := UserListResponseEntity{}
	userList.Code = 200
	userList.Data.Total = rsp.Total
	for _, value := range rsp.Users {

		var entity UserInfoResponseEntity
		entity.UserId = value.UserId
		entity.UserName = value.UserName
		entity.UserEmail = value.UserEmail
		entity.UserPhoneNum = value.UserPhoneNum
		entity.UserEnable = int(value.UserEnable)
		entity.CreatedAt = value.CreateTime
		entity.LoginTime = value.LoginTime
		entity.UserRole = value.UserRole
		entity.UserIntroduction = value.UserIntroduction
		userList.Data.Rows = append(userList.Data.Rows, entity)
	}
	Success(c, int(userList.Code), userList.Msg, userList.Data)
	return
}

// UserRoleCreateHandler    @Summary
// @Description 新增角色
// @Id 38
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param roleName formData string false "角色名称"
// @Param roleIdName formData string false "角色ID名称"
// @Param roleDes formData string false "角色描述"
// @Param roleEnable formData int false "角色是否可用状态 0不可以用 1可用"
// @Success 200 object BaseResponseEntity "成功"
// @Failure 3000 object BaseResponseEntity "失败"
// @Router /user/v1/userRole/createRole [post]
func UserRoleCreateHandler(c *gin.Context) {

	var roleInfo RoleInfoForm

	if err := c.ShouldBindJSON(&roleInfo); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, err := AccountServiceClient.CreatUserRole(context.TODO(), &accountCenter.CreateUserRoleRequest{
		RoleNum:    roleInfo.RoleNum,
		RoleName:   roleInfo.RoleName,
		RoleIdName: roleInfo.RoleIdName,
		RoleDes:    roleInfo.RoleDes,
		RoleEnable: int64(roleInfo.RoleEnable),
	})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	Success(c, int(rsp.Code), rsp.Msg, nil)
}

// RoleListHandler    @Summary
// @Description 角色列表
// @Id 39
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param filterByRoleId formData string false "按用户名称过滤"
// @Success 200 object RoleListResponseEntity "成功"
// @Failure 400 object RoleListResponseEntity "失败"
// @Router /user/v1/userRole/roleList [get]
func RoleListHandler(c *gin.Context) {

	var getListForm RoleInfoListForm

	if err := c.ShouldBind(&getListForm); err != nil {
		log.Println("err ->", err.Error())
	}

	rsp, err := AccountServiceClient.QueryRoles(context.TODO(), &accountCenter.QueryRolesRequest{})

	if err != nil {
		log.Print(err.Error())
		Err(c, -1, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	roleList := RoleListResponseEntity{}
	roleList.Code = 200
	roleList.Data.Total = rsp.Total
	timeLayout := "2006-01-02 15:04:05"
	for _, value := range rsp.Roles {

		var entity RoleListRowDetailEntity
		entity.RoleId = value.RoleId
		entity.RoleIdName = value.RoleIdName
		entity.RoleName = value.RoleName
		entity.RoleDes = value.RoleDes
		entity.RoleEnable = value.RoleEnable
		entity.RoleCreateAt = time.Unix(value.CreateTime, 0).Format(timeLayout)
		roleList.Data.Rows = append(roleList.Data.Rows, entity)
	}
	Success(c, int(roleList.Code), roleList.Msg, roleList.Data)
	return
}

// UpdateRoleInfoHandler       @Summary
// @Description 修改角色信息
// @Id 40
// @Tags 用户中心
// @version 1.0
// @Accept application/x-json-stream
// @Param roleId formData string true " 角色ID"
// @Param roleName formData string false "角色名称"
// @Param roleIdName formData string false "角色ID名称"
// @Param roleDes formData string false "角色简介"
// @Param roleEnable formData int false "角色是否可用状态 0不可以用 1可用"
// @Success 200 object BaseResponseEntity "修改成功"
// @Failure 4000 object BaseResponseEntity "修改失败"
// @Router /user/v1/userRole/updateRoleRole [post]
func UpdateRoleInfoHandler(c *gin.Context) {

	var roleInfo RoleInfoForm

	if err := c.ShouldBind(&roleInfo); err != nil {
		log.Println("err ->", err.Error())
	}
	updateRsp, error := AccountServiceClient.UpdateRole(context.TODO(), &accountCenter.UpdateRoleRequest{
		RoleId:     roleInfo.RoleId,
		RoleIdName: roleInfo.RoleIdName,
		RoleName:   roleInfo.RoleName,
		RoleDes:    roleInfo.RoleDes,
		RoleEnable: int64(roleInfo.RoleEnable),
	})

	if error != nil {
		log.Print(error.Error())
		Err(c, -1, http.StatusInternalServerError, error.Error(), nil)
		return
	}

	Success(c, int(updateRsp.Code), updateRsp.Msg, nil)

	return

}
