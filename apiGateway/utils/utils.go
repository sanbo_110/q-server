package utils

import (
	. "apiGateway/forms"
	md "apiGateway/middleware"
	jwtGo "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"time"
)

func ResolveTime(seconds int) (hour int, minute int, second int) {
	hour = seconds / 1000 / 60 / 60 % 24
	minute = seconds / 1000 / 60 % 60
	second = seconds / 1000 % 60
	return
}

// GenerateToken token生成器
func GenerateToken(c *gin.Context, user UserInfoForm) (string, error) {
	// 构造SignKey: 签名和解签名需要使用一个值
	j := md.NewJWT()

	// 构造用户claims信息(负荷)

	claims := md.CustomClaims{
		ID: user.UserPhoneNum,
		StandardClaims: jwtGo.StandardClaims{
			NotBefore: int64(time.Now().Unix() - 1000),
			ExpiresAt: int64(time.Now().Unix() + 3600*24*7),
			Issuer:    "tme.top",
		},
	}

	// 根据claims生成token对象
	token, err := j.CreateToken(claims)

	return token, err
}
