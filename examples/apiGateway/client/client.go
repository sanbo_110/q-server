package client

import (
	"apiGateway/handler"
	proto "apiGateway/proto"
	_ "github.com/asim/go-micro/plugins/registry/kubernetes/v3"
	"github.com/asim/go-micro/v3"
	"github.com/micro/cli/v2"
)

var Port string

const (
	TaskServiceName = "TaskService"
)

func RegisterService() {
	service := micro.NewService(
		micro.Flags(
			&cli.StringFlag{
				Name:  "p",
				Usage: "port",
			},
		),
	)

	service.Init(
		micro.Action(func(c *cli.Context) error {
			Port = c.String("p")
			if len(Port) == 0 {
				Port = "8080"
			}
			return nil
		},
		),
	)

	cli := service.Client()

	handler.TaskServiceClient = proto.NewTaskService(TaskServiceName, cli)
}