package handler

import (
	proto "apiGateway/proto"
	"context"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
)

var (
	TaskServiceClient proto.TaskService
)

func AddTaskHandler(c *gin.Context) {
	taskName := c.PostForm("taskName")
	taskType := c.PostForm("taskType")

	taskTypeInt, _ := strconv.Atoi(taskType)

	rsp, err := TaskServiceClient.AddTask(context.TODO(), &proto.AddRequest{
		TaskName: taskName,
		TaskType: int64(taskTypeInt),
	})

	if err != nil {
		log.Print(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": -1,
			"message": "server error",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"taskId": rsp.TaskId,
		"taskName": rsp.TaskName,
	})
	return
}