package config

import (
	"github.com/timest/env"
)

type Config struct {
	MySQL struct {
		Host    	string
		Port    	int
		User    	string
		Password    string
	}
}

var Cfg *Config

func init() {
	Cfg = new(Config)
	env.IgnorePrefix()
	err := env.Fill(Cfg)
	if err != nil {
		panic(err)
	}
}