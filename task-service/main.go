package main

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"task-service/core"
	"task-service/handler"
	pb "task-service/proto"

	_ "github.com/asim/go-micro/plugins/registry/kubernetes/v3"
	"github.com/asim/go-micro/v3"
	log "github.com/asim/go-micro/v3/logger"
)

var (
	service = "task-service"
	version = "latest"
)

func main() {
	// MongoDB
	mongoClient, db, err := core.MongoDB(context.Background())
	if err != nil {
		log.Fatal("MongoDB connect error: %v", err)
		return
	}
	defer func(client *mongo.Client, ctx context.Context) {
		err := client.Disconnect(ctx)
		if err != nil {
			log.Fatal("MongoDB disconnect error: %v", err)
		}
	}(mongoClient, context.Background())
	store := db.Collection("task")
	StoreSnapshot := db.Collection("snapshot")
	StoreLog := db.Collection("log")

	// Create service
	srv := micro.NewService(
		micro.Name(service),
		micro.Version(version),
	)
	srv.Init()

	// Register handler
	err = pb.RegisterAudioServiceHandler(srv.Server(), &handler.AudioService{
		Store: store,
	})
	if err != nil {
		log.Fatal(err)
	}
	err = pb.RegisterTranslateServiceHandler(srv.Server(), &handler.TranslateService{
		Store: store,
	})
	if err != nil {
		log.Fatal(err)
	}
	err = pb.RegisterSnapshotServiceHandler(srv.Server(), &handler.SnapshotService{
		StoreTask:     store,
		StoreSnapshot: StoreSnapshot,
	})
	if err != nil {
		log.Fatal(err)
	}
	err = pb.RegisterReviewServiceHandler(srv.Server(), &handler.ReviewService{
		StoreTask: store,
	})
	if err != nil {
		log.Fatal(err)
	}
	err = pb.RegisterLogServiceHandler(srv.Server(), &handler.LogService{
		StoreLog: StoreLog,
	})
	if err != nil {
		log.Fatal(err)
	}
	err = pb.RegisterStatisticServiceHandler(srv.Server(), &handler.StatisticService{
		StoreTask: store,
	})
	if err != nil {
		log.Fatal(err)
	}

	// Run service
	if err := srv.Run(); err != nil {
		log.Fatal(err)
	}
}
