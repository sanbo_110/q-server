package handler

import (
	"context"
	log "github.com/asim/go-micro/v3/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"strconv"
	"task-service/core"
	"task-service/model"
	pb "task-service/proto"
	"time"
	"unicode/utf8"
)

type TranslateService struct {
	Store *mongo.Collection
}

func (e *TranslateService) Trigger(ctx context.Context, req *pb.TriggerRequest, rsp *pb.TriggerResponse) error {
	log.Debugf("TranslateService.Trigger request: %v", req)

	audioId := req.GetAudioId()
	objectID, err := primitive.ObjectIDFromHex(audioId)
	if err != nil {
		log.Errorf("TranslateService.Trigger error: %v", err)
		return err
	}

	var result struct {
		AudioUri       string `bson:"audio_uri"`
		AudioState     int64  `bson:"audio_state,omitempty"`
		TranslateState int64  `bson:"translate_state,omitempty"`
	}
	err = e.Store.FindOne(context.Background(), bson.M{"_id": objectID}).Decode(&result)
	if err != nil {
		log.Errorf("TranslateService.Trigger error: %v", err)
		return err
	}

	//状态校验
	if result.TranslateState != model.TranslateStateWaiting {
		log.Errorf("TranslateService.Trigger error: translate state %v", result.TranslateState)
		return err
	}

	//转换
	response, err := core.YTCreateTranslate(result.AudioUri)
	if err != nil {
		log.Errorf("TranslateService.Trigger error: %v", err)
		return err
	}
	log.Infof("TranslateService.Trigger data: %+v", response)

	if response.Rtn != 0 {
		data := bson.M{"$set": bson.M{
			"translate_task_id":      response.TaskId,
			"translate_state":        model.TranslateStateFailed,
			"translate_completed_at": time.Now().Unix(),
		}}
		_, _ = e.Store.UpdateByID(context.Background(), objectID, data)
		return nil
	}

	//存储
	data := bson.M{"$set": bson.M{
		"translate_task_id": response.TaskId,
		"translate_join_at": time.Now().Unix(),
		"translate_state":   model.TranslateStateJoined,
	}}
	log.Infof("TranslateService.Trigger data: %v", data)
	_, err = e.Store.UpdateByID(context.Background(), objectID, data)
	if err != nil {
		log.Errorf("TranslateService.Trigger error: %v", err)
		return err
	}

	log.Debugf("TranslateService.Trigger response: %v", rsp)
	return nil
}

func (e *TranslateService) Query(ctx context.Context, req *pb.TQueryRequest, rsp *pb.TQueryResponse) error {
	log.Debugf("TranslateService.Query request: %v", req)

	audioId := req.GetAudioId()
	objectID, err := primitive.ObjectIDFromHex(audioId)
	if err != nil {
		log.Errorf("TranslateService.Query error: %v", err)
		return err
	}

	var result struct {
		TranslateTaskId string `bson:"translate_task_id,omitempty"`
		TranslateState  int64  `bson:"translate_state,omitempty"`
	}
	err = e.Store.FindOne(context.Background(), bson.M{"_id": objectID}).Decode(&result)
	if err != nil {
		log.Errorf("TranslateService.Query error: %v", err)
		return err
	}

	//状态校验
	if result.TranslateState != model.TranslateStateJoined {
		log.Errorf("TranslateService.Trigger error: translate state %v", result.TranslateState)
		return err
	}

	//获取转换结果
	response, err := core.YTQueryTranslate(result.TranslateTaskId)
	if err != nil {
		log.Errorf("TranslateService.Trigger error: %v", err)
		return err
	}
	log.Infof("TranslateService.Query data: %+v", response)

	if response.Rtn != 0 {
		data := bson.M{"$set": bson.M{
			"translate_state":        model.TranslateStateFailed,
			"translate_completed_at": time.Now().Unix(),
		}}
		_, _ = e.Store.UpdateByID(context.Background(), objectID, data)
		return nil
	}

	if response.Data.StatusText != "TASK_SUCC" {
		return nil
	}

	var rows []model.TranslateResult

	for _, detail := range response.Data.SpeechResult.Detail {
		var row model.TranslateResult
		row.Sentences = detail.Sentences
		row.StartTime, _ = strconv.ParseInt(detail.StartTime, 10, 64)
		row.EndTime, _ = strconv.ParseInt(detail.EndTime, 10, 64)
		rows = append(rows, row)
	}

	//存储
	data := bson.M{"$set": bson.M{
		"audio_total_words":      utf8.RuneCountInString(response.Data.SpeechResult.ResultText),
		"audio_length":           response.Data.SpeechResult.Duration,
		"translate_state":        model.TranslateStateCompleted,
		"translate_result":       rows,
		"translate_completed_at": time.Now().Unix(),
	}}
	log.Infof("TranslateService.Query data: %v", data)
	_, err = e.Store.UpdateByID(context.Background(), objectID, data)
	if err != nil {
		log.Errorf("TranslateService.Query error: %v", err)
		return err
	}

	log.Debugf("TranslateService.Query response: %v", rsp)
	return nil
}
