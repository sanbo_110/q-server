package handler

import (
	"context"
	log "github.com/asim/go-micro/v3/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"task-service/model"
	pb "task-service/proto"
	"time"
)

type LogService struct {
	StoreLog *mongo.Collection
}

func (e *LogService) LogReceive(
	ctx context.Context,
	req *pb.LogReceiveRequest,
	rsp *pb.LogReceiveResponse,
) error {
	log.Debugf("LogReceive request: %v", req)

	id := req.GetId()
	if id == "" {
		id = primitive.NewObjectID().Hex()
	}

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Errorf("LogReceive error: %v", err)
		return err
	}

	at := req.GetAt()
	if at == 0 {
		at = time.Now().Unix()
		req.At = &at
	}

	category := req.GetCategory()
	message := req.GetMessage()
	userId := req.GetUserId()
	audioId := req.GetAudioId()
	audioName := req.GetAudioName()
	params := req.GetParams()

	data := &model.LogCollection{
		Id:        objectId,
		Category:  category,
		At:        at,
		Message:   message,
		UserId:    userId,
		AudioId:   audioId,
		AudioName: audioName,
		Params:    params,
	}

	result, err := e.StoreLog.InsertOne(ctx, data)
	if err != nil {
		log.Errorf("AudioService.Create error: %v", err)
		return err
	}
	InsertedID := result.InsertedID.(primitive.ObjectID).Hex()
	rsp.Id = InsertedID

	log.Debugf("LogReceive response: %v", rsp)
	return nil
}

func (e *LogService) LogList(
	ctx context.Context,
	req *pb.LogListRequest,
	rsp *pb.LogListResponse,
) error {
	log.Debugf("LogList request: %v", req)

	reqLimit := req.GetLimit()
	if reqLimit == 0 {
		reqLimit = 30
	}
	rsp.Limit = reqLimit

	reqPage := req.GetPage()
	if reqPage < 1 {
		reqPage = 1
	}
	rsp.Page = reqPage

	reqFilter := req.GetFilter()

	filter := bson.D{}

	reqFilterCategory := reqFilter.GetCategory()
	if reqFilterCategory != 0 {
		filter = append(filter, bson.E{
			Key:   "category",
			Value: reqFilterCategory,
		})
	}

	reqFilterAt := reqFilter.GetAt()
	if reqFilterAt != nil {
		filter = append(filter, bson.E{Key: "at", Value: bson.M{
			"$gt": reqFilterAt.Start,
			"$lt": reqFilterAt.End,
		}})
	}

	reqFilterAudioName := reqFilter.GetAudioName()
	if reqFilterAudioName != "" {
		filter = append(filter, bson.E{
			Key:   "audio_name",
			Value: bson.M{"$regex": reqFilterAudioName}},
		)
	}

	total, err := e.StoreLog.CountDocuments(ctx, filter)
	if err != nil {
		log.Errorf("LogList error: %v", err)
		return err
	}
	rsp.Total = total

	sort := bson.D{}
	reqSort := req.GetSort()
	reqSortAt := reqSort.GetAt()
	if reqSortAt != pb.LogListRequest_Sort_Def {
		sort = append(sort, bson.E{"at", reqSortAt})
	}

	cursor, err := e.StoreLog.Find(
		ctx,
		filter,
		options.Find().SetSkip((reqPage-1)*reqLimit),
		options.Find().SetLimit(reqLimit),
		options.Find().SetSort(sort),
	)
	if err != nil {
		log.Errorf("LogList error: %v", err)
		return err
	}

	defer func(cursor *mongo.Cursor, ctx context.Context) {
		_ = cursor.Close(ctx)
	}(cursor, ctx)

	for cursor.Next(context.Background()) {
		row := model.LogCollection{}
		err := cursor.Decode(&row)
		if err != nil {
			log.Errorf("LogList error: %v", err)
			return err
		}
		rsp.Rows = append(rsp.Rows, &pb.LogListResponseRow{
			Id:        row.Id.Hex(),
			Category:  row.Category,
			At:        row.At,
			Message:   row.Message,
			UserId:    row.UserId,
			AudioId:   row.AudioId,
			AudioName: row.AudioName,
		})
	}
	if err := cursor.Err(); err != nil {
		return err
	}

	log.Debugf("LogList response: %v", rsp)
	return nil
}
