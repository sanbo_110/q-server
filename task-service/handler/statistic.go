package handler

import (
	"context"
	log "github.com/asim/go-micro/v3/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"task-service/model"
	pb "task-service/proto"
)

type StatisticService struct {
	StoreTask *mongo.Collection
}

func (e *StatisticService) PlatformTotalTranslateAudioLength(
	ctx context.Context,
	req *pb.StatisticEmptyRequest,
	rsp *pb.StatisticIntValueRequest,
) error {
	log.Debugf("QuantityUserAudited request: %v", req)

	match := bson.M{}
	match["translate_state"] = model.TranslateStateCompleted

	group := bson.D{}
	group = append(group, bson.E{Key: "_id", Value: nil})
	group = append(group, bson.E{Key: "value", Value: bson.D{{"$sum", "$audio_length"}}})

	//分组条件
	groupStage := []bson.M{
		{"$match": match},
		{"$group": group},
	}
	//数据查询
	cursor, err := e.StoreTask.Aggregate(
		context.TODO(),
		groupStage,
	)
	if err != nil {
		log.Errorf("QuantityUserAudited error: %v", err)
		return err
	}
	defer func(cursor *mongo.Cursor, ctx context.Context) {
		_ = cursor.Close(ctx)
	}(cursor, ctx)
	//结果处理
	var row bson.D
	if err = cursor.All(context.TODO(), &row); err != nil {
		log.Fatal(err)
	}
	rsp.Value = row[0].Value.(int64)

	log.Debugf("QuantityUserAudited response: %v", rsp)
	return nil
}

func (e *StatisticService) QuantityUserAudited(
	ctx context.Context,
	req *pb.StatisticUserIdRequest,
	rsp *pb.StatisticUserIdValueRequest,
) error {
	log.Debugf("QuantityUserAudited request: %v", req)

	match := bson.M{"release_at": bson.M{"$exists": true}}
	reqUserIds := req.GetUserIds()
	if len(reqUserIds) == 0 {
		match["dispatch_by"] = bson.M{"$exists": true}
	} else {
		match["dispatch_by"] = bson.M{"$in": reqUserIds}
	}

	group := bson.D{}
	group = append(group, bson.E{Key: "_id", Value: "$dispatch_by"})
	group = append(group, bson.E{Key: "quantity", Value: bson.D{{"$sum", 1}}})

	//分组条件
	groupStage := []bson.M{
		{"$match": match},
		{"$group": group},
	}
	//数据查询
	cursor, err := e.StoreTask.Aggregate(
		context.TODO(),
		groupStage,
	)
	if err != nil {
		log.Errorf("QuantityUserAudited error: %v", err)
		return err
	}
	defer func(cursor *mongo.Cursor, ctx context.Context) {
		_ = cursor.Close(ctx)
	}(cursor, ctx)
	//结果处理
	var rows []bson.M
	if err = cursor.All(context.TODO(), &rows); err != nil {
		log.Fatal(err)
	}
	rsp.Values = make(map[string]int64)
	for _, row := range rows {
		userId := row["_id"].(string)
		quantity := row["quantity"].(int32)
		rsp.Values[userId] = int64(quantity)
	}

	log.Debugf("QuantityUserAudited response: %v", rsp)
	return nil
}

func (e *StatisticService) QuantityUserAuditing(
	ctx context.Context,
	req *pb.StatisticUserIdRequest,
	rsp *pb.StatisticUserIdValueRequest,
) error {
	log.Debugf("QuantityUserAuditing request: %v", req)

	match := bson.M{"release_at": bson.M{"$exists": false}}
	reqUserIds := req.GetUserIds()
	if len(reqUserIds) == 0 {
		match["dispatch_by"] = bson.M{"$exists": true}
	} else {
		match["dispatch_by"] = bson.M{"$in": reqUserIds}
	}

	group := bson.D{}
	group = append(group, bson.E{Key: "_id", Value: "$dispatch_by"})
	group = append(group, bson.E{Key: "quantity", Value: bson.D{{"$sum", 1}}})

	//分组条件
	groupStage := []bson.M{
		{"$match": match},
		{"$group": group},
	}
	//数据查询
	cursor, err := e.StoreTask.Aggregate(
		context.TODO(),
		groupStage,
	)
	if err != nil {
		log.Errorf("QuantityUserAudited error: %v", err)
		return err
	}
	defer func(cursor *mongo.Cursor, ctx context.Context) {
		_ = cursor.Close(ctx)
	}(cursor, ctx)
	//结果处理
	var rows []bson.M
	if err = cursor.All(context.TODO(), &rows); err != nil {
		log.Fatal(err)
	}
	rsp.Values = make(map[string]int64)
	for _, row := range rows {
		userId := row["_id"].(string)
		quantity := row["quantity"].(int32)
		rsp.Values[userId] = int64(quantity)
	}

	log.Debugf("QuantityUserAuditing response: %v", rsp)
	return nil
}
