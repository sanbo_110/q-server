package handler

import (
	"context"
	log "github.com/asim/go-micro/v3/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"task-service/model"
	pb "task-service/proto"
	"time"
)

type SnapshotService struct {
	StoreTask     *mongo.Collection
	StoreSnapshot *mongo.Collection
}

func (e *SnapshotService) SnapshotCover(
	ctx context.Context,
	req *pb.SnapshotCoverRequest,
	rsp *pb.SnapshotCoverResponse,
) error {
	log.Debugf("SnapshotCover request: %v", req)

	snapshotId := req.GetSnapshotId()
	if snapshotId == "" {
		log.Errorf("SnapshotCover error: %v", "缺少 SnapshotId 参数")
		return nil
	}
	snapshotObjectID, err := primitive.ObjectIDFromHex(snapshotId)
	if err != nil {
		log.Errorf("SnapshotCover error: %v", err)
		return err
	}

	isRelease := req.GetIsRelease()

	snapshotAt := req.GetSnapshotAt()
	if snapshotAt == 0 {
		snapshotAt = time.Now().Unix()
		req.SnapshotAt = &snapshotAt
	}

	result := model.SnapshotCollection{}
	err = e.StoreSnapshot.FindOne(ctx, bson.M{"_id": snapshotObjectID}).Decode(&result)
	if err != nil {
		log.Errorf("SnapshotCover error: %v", "获取快照数据失败")
		log.Errorf("SnapshotCover error: %v", err)
		return err
	}

	var SnapshotDiffResult []model.SnapshotDiffResult
	updateRows := req.GetUpdateRows()
	if updateRows != nil {
		for _, row := range updateRows {
			key := row.Key
			index := key - 1
			newSentences := row.Sentences
			item := result.SnapshotResult[index]
			oldSentences := item.Sentences
			result.SnapshotResult[index].Sentences = newSentences
			SnapshotDiffResult = append(SnapshotDiffResult, model.SnapshotDiffResult{
				Key:          key,
				StartTime:    item.StartTime,
				EndTime:      item.EndTime,
				OldSentences: oldSentences,
				NewSentences: newSentences,
			})
		}
	} else {
		SnapshotDiffResult = result.SnapshotDiffResult
	}

	//写入音频
	data := bson.M{}
	if isRelease == 1 {
		data = bson.M{"$set": bson.M{
			"snapshot_result": result.SnapshotResult,
			"release_result":  result.SnapshotResult,
		}}
	} else {
		data = bson.M{"$set": bson.M{
			"snapshot_result": result.SnapshotResult,
		}}
	}
	taskObjectId, err := primitive.ObjectIDFromHex(result.AudioId)
	if err != nil {
		log.Errorf("SnapshotDetail error: %v", err)
		return err
	}
	_, err = e.StoreTask.UpdateByID(context.Background(), taskObjectId, data)
	if err != nil {
		log.Errorf("SnapshotCover error: %v", "音频数据保存失败")
		log.Errorf("SnapshotCover error: %v", err)
		return err
	}
	//创建版本
	diffRows := int64(len(SnapshotDiffResult))
	id := primitive.NewObjectID()
	var insertData = &model.SnapshotCollection{
		SnapshotId:         id,
		SnapshotBy:         req.GetSnapshotBy(),
		SnapshotAt:         req.GetSnapshotAt(),
		SnapshotDiffRows:   diffRows,
		SnapshotDiffResult: SnapshotDiffResult,
		SnapshotResult:     result.SnapshotResult,
		SnapshotIsRelease:  isRelease,
		AudioId:            result.AudioId,
		AudioName:          result.AudioName,
	}
	insertResult, err := e.StoreSnapshot.InsertOne(context.Background(), insertData)
	if err != nil {
		log.Errorf("SnapshotCover error: %v", err)
		return err
	}
	InsertedID := insertResult.InsertedID.(primitive.ObjectID).Hex()
	rsp.SnapshotId = InsertedID

	log.Debugf("SnapshotCover response: %v", rsp)
	return nil
}

func (e *SnapshotService) SnapshotDetail(
	ctx context.Context,
	req *pb.SnapshotDetailRequest,
	rsp *pb.SnapshotDetailResponse,
) error {
	log.Debugf("SnapshotDetail request: %v", req)

	snapshotId := req.GetSnapshotId()
	snapshotObjectID, err := primitive.ObjectIDFromHex(snapshotId)
	if err != nil {
		log.Errorf("SnapshotDetail error: %v", err)
		return err
	}

	snapshotResult := model.SnapshotCollection{}
	err = e.StoreSnapshot.FindOne(ctx, bson.M{"_id": snapshotObjectID}).Decode(&snapshotResult)
	if err != nil {
		log.Errorf("SnapshotDetail error: %v", "快照结果为空")
		log.Errorf("SnapshotDetail error: %v", err)
		return err
	}

	taskObjectId, err := primitive.ObjectIDFromHex(snapshotResult.AudioId)
	if err != nil {
		log.Errorf("SnapshotDetail error: %v", err)
		return err
	}
	taskResult := model.TaskCollection{}
	err = e.StoreTask.FindOne(ctx, bson.M{"_id": taskObjectId}).Decode(&taskResult)
	if err != nil {
		log.Errorf("SnapshotDetail error: %v", "快照结果为空")
		log.Errorf("SnapshotDetail error: %v", err)
		return err
	}

	rsp.SnapshotId = snapshotResult.SnapshotId.Hex()
	rsp.SnapshotAt = snapshotResult.SnapshotAt
	rsp.SnapshotBy = snapshotResult.SnapshotBy
	rsp.SnapshotIsRelease = snapshotResult.SnapshotIsRelease
	rsp.SnapshotDiffRows = snapshotResult.SnapshotDiffRows
	rsp.AudioId = taskResult.AudioId.Hex()
	rsp.AudioName = taskResult.AudioName
	rsp.AudioLength = taskResult.AudioLength
	rsp.AudioCreatedAt = taskResult.AudioCreatedAt
	rsp.AudioCreatedBy = taskResult.AudioCreatedBy
	rsp.AudioState = taskResult.AudioState
	rsp.AudioTotalWords = taskResult.AutoTotalWords

	var rows []*pb.SnapshotDiffResult
	for _, value := range snapshotResult.SnapshotDiffResult {
		var row pb.SnapshotDiffResult
		row.OldSentences = value.OldSentences
		row.NewSentences = value.NewSentences
		row.Key = value.Key
		row.StartTime = value.StartTime
		row.EndTime = value.EndTime
		rows = append(rows, &row)
	}
	rsp.SnapshotDiffResult = rows

	log.Debugf("SnapshotDetail response: %v", rsp)
	return nil
}

func (e *SnapshotService) SnapshotQuery(
	ctx context.Context,
	req *pb.SnapshotQueryRequest,
	rsp *pb.SnapshotQueryResponse,
) error {
	log.Debugf("SnapshotQuery request: %v", req)

	limit := req.GetLimit()
	if limit == 0 {
		limit = 30
	}
	rsp.Limit = limit

	page := req.GetPage()
	if page < 1 {
		page = 1
	}
	rsp.Page = page

	sortField := req.GetSortField()
	if sortField == "" {
		sortField = "snapshot_at"
	}

	sortMode := req.GetSortMode()
	if sortMode == 0 {
		sortMode = -1
	}

	filter := bson.D{}

	filterName := req.GetFilterName()
	if filterName != "" {
		filter = append(filter, bson.E{Key: "audio_name", Value: bson.M{"$regex": filterName}})
	}

	filterCreatedAt := req.GetFilterSnapshotAt()
	if filterCreatedAt != nil {
		filter = append(filter, bson.E{Key: "snapshot_at", Value: bson.M{
			"$gt": filterCreatedAt.Start,
			"$lt": filterCreatedAt.End,
		}})
	}

	total, err := e.StoreSnapshot.CountDocuments(ctx, filter)
	if err != nil {
		log.Errorf("SnapshotQuery error: %v", err)
		return err
	}
	rsp.Total = total

	cursor, err := e.StoreSnapshot.Find(
		ctx,
		filter,
		options.Find().SetSkip((page-1)*limit),
		options.Find().SetLimit(limit),
		options.Find().SetSort(bson.M{sortField: sortMode}),
	)
	if err != nil {
		log.Errorf("SnapshotQuery error: %v", err)
		return err
	}

	defer func(cursor *mongo.Cursor, ctx context.Context) {
		_ = cursor.Close(ctx)
	}(cursor, ctx)

	for cursor.Next(context.Background()) {
		result := model.SnapshotCollection{}
		err := cursor.Decode(&result)
		if err != nil {
			log.Errorf("MongoDB cursor close error: %v", err)
			return err
		}
		rsp.Rows = append(rsp.Rows, &pb.SnapshotQueryRowResponse{
			SnapshotId:        result.SnapshotId.Hex(),
			SnapshotAt:        result.SnapshotAt,
			SnapshotBy:        result.SnapshotBy,
			SnapshotDiffRows:  result.SnapshotDiffRows,
			SnapshotIsRelease: result.SnapshotIsRelease,
			AudioId:           result.AudioId,
			AudioName:         result.AudioName,
		})
	}
	if err := cursor.Err(); err != nil {
		return err
	}

	log.Debugf("SnapshotQuery response: %v", rsp)
	return nil
}

func (e *SnapshotService) SnapshotSubmit(
	ctx context.Context,
	req *pb.SnapshotSubmitRequest,
	rsp *pb.SnapshotSubmitResponse,
) error {
	log.Debugf("SnapshotSubmit request: %v", req)

	audioId := req.GetAudioId()
	if audioId == "" {
		log.Errorf("SnapshotSubmit error: %v", "缺少 AudioId 参数")
		return nil
	}
	audioObjectID, err := primitive.ObjectIDFromHex(audioId)
	if err != nil {
		log.Errorf("SnapshotSubmit error: %v", err)
		return err
	}

	updateRows := req.GetUpdateRows()
	if updateRows == nil {
		log.Errorf("SnapshotSubmit error: %v", "缺少 UpdateRows 参数")
		return nil
	}

	isRelease := req.GetIsRelease()

	snapshotAt := req.GetSnapshotAt()
	if snapshotAt == 0 {
		snapshotAt = time.Now().Unix()
		req.SnapshotAt = &snapshotAt
	}

	result := model.TaskCollection{}
	err = e.StoreTask.FindOne(ctx, bson.M{"_id": audioObjectID}).Decode(&result)
	if err != nil {
		log.Errorf("SnapshotSubmit error: %v", "获取音频数据失败")
		log.Errorf("SnapshotSubmit error: %v", err)
		return err
	}

	var SnapshotResult []model.TranslateResult
	var SnapshotDiffResult []model.SnapshotDiffResult
	if result.SnapshotResult == nil {
		SnapshotResult = result.TranslateResult
	} else {
		SnapshotResult = result.SnapshotResult
	}

	for _, row := range updateRows {
		key := row.Key
		index := key - 1
		newSentences := row.Sentences
		item := SnapshotResult[index]
		oldSentences := item.Sentences
		SnapshotResult[index].Sentences = newSentences
		SnapshotDiffResult = append(SnapshotDiffResult, model.SnapshotDiffResult{
			Key:          key,
			OldSentences: oldSentences,
			NewSentences: newSentences,
			StartTime:    item.StartTime,
			EndTime:      item.EndTime,
		})
	}

	//写入音频
	data := bson.M{}
	if isRelease == 1 {
		data = bson.M{"$set": bson.M{
			"snapshot_result": SnapshotResult,
			"snapshot_at":     time.Now().Unix(),
			"release_result":  SnapshotResult,
			"release_at":      time.Now().Unix(),
			"release_state":   true,
		}}
	} else {
		data = bson.M{"$set": bson.M{
			"snapshot_result": SnapshotResult,
			"snapshot_at":     time.Now().Unix(),
			"release_state":   false,
		}}
	}
	_, err = e.StoreTask.UpdateByID(context.Background(), audioObjectID, data)
	if err != nil {
		log.Errorf("SnapshotSubmit error: %v", "音频数据保存失败")
		log.Errorf("SnapshotSubmit error: %v", err)
		return err
	}
	//创建版本
	diffRows := int64(len(SnapshotDiffResult))
	id := primitive.NewObjectID()
	var insertData = &model.SnapshotCollection{
		SnapshotId:         id,
		SnapshotBy:         req.GetSnapshotBy(),
		SnapshotAt:         req.GetSnapshotAt(),
		SnapshotDiffRows:   diffRows,
		SnapshotDiffResult: SnapshotDiffResult,
		SnapshotResult:     SnapshotResult,
		SnapshotIsRelease:  isRelease,
		AudioId:            audioId,
		AudioName:          result.AudioName,
	}
	insertResult, err := e.StoreSnapshot.InsertOne(context.Background(), insertData)
	if err != nil {
		log.Errorf("SnapshotSubmit error: %v", err)
		return err
	}
	InsertedID := insertResult.InsertedID.(primitive.ObjectID).Hex()
	rsp.SnapshotId = InsertedID

	log.Debugf("SnapshotSubmit response: %v", rsp)
	return nil
}
