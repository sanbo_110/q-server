package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type TaskCollection struct {
	AudioId              primitive.ObjectID `bson:"_id"`
	AudioName            string             `bson:"audio_name"`
	AudioUri             string             `bson:"audio_uri"`
	AudioLength          int64              `bson:"audio_length,omitempty"`
	AutoTotalWords       int64              `bson:"audio_total_words,omitempty"`
	AudioCreatedAt       int64              `bson:"audio_created_at,omitempty"`
	AudioCreatedBy       string             `bson:"audio_created_by,omitempty"`
	AudioPriority        int64              `bson:"audio_priority,omitempty"`
	AudioState           int64              `bson:"audio_state,omitempty"`
	TranslateJoinAt      int64              `bson:"translate_join_at,omitempty"`
	TranslateJoinBy      string             `bson:"translate_join_by,omitempty"`
	TranslateCompletedAt int64              `bson:"translate_completed_at,omitempty"`
	TranslateState       int64              `bson:"translate_state,omitempty"`
	TranslateTaskId      string             `bson:"translate_task_id,omitempty"`
	TranslateResult      []TranslateResult  `bson:"translate_result,omitempty"`
	SnapshotResult       []TranslateResult  `bson:"snapshot_result,omitempty"`
	SnapshotAt           int64              `bson:"snapshot_at,omitempty"`
	ReleaseResult        []TranslateResult  `bson:"release_result,omitempty"`
	ReleaseAt            int64              `bson:"release_at,omitempty"`
	ReleaseState         bool               `bson:"release_state,omitempty"`
	DispatchAt           int64              `bson:"dispatch_at,omitempty"`
	DispatchBy           string             `bson:"dispatch_by,omitempty"`
}

const (
	AudioStateCreated int64 = 1
	AudioStateDeleted int64 = 2
)

const (
	TranslateStateWaiting   int64 = 1
	TranslateStateJoined    int64 = 2
	TranslateStateCompleted int64 = 3
	TranslateStateFailed    int64 = 4
)
