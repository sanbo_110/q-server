package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type SnapshotCollection struct {
	SnapshotId         primitive.ObjectID   `bson:"_id"`
	SnapshotBy         string               `bson:"snapshot_by"`
	SnapshotAt         int64                `bson:"snapshot_at"`
	SnapshotDiffRows   int64                `bson:"snapshot_diff_rows"`
	SnapshotDiffResult []SnapshotDiffResult `bson:"snapshot_diff_result"`
	SnapshotResult     []TranslateResult    `bson:"snapshot_result"`
	SnapshotIsRelease  int64                `bson:"snapshot_is_release"`
	AudioId            string               `bson:"audio_id"`
	AudioName          string               `bson:"audio_name"`
}

type SnapshotDiffResult struct {
	Key          int64  `bson:"key"`
	StartTime    int64  `bson:"start_time"`
	EndTime      int64  `bson:"end_time"`
	OldSentences string `bson:"old_sentences"`
	NewSentences string `bson:"new_sentences"`
}
