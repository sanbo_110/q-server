package core

import (
	"context"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func MongoDB(ctx context.Context) (*mongo.Client, *mongo.Database, error) {
	var db *mongo.Database
	uri := os.Getenv("DB_URI")
	if uri == "" {
		uri = "mongodb://8.140.173.110:27017"
		//uri = "mongodb://127.0.0.1:27017"
	}
	name := os.Getenv("DB_NAME")
	if name == "" {
		name = "local"
	}
	client, err := MongoConnect(ctx, uri, 0)
	if err != nil {
		return nil, nil, err
	} else {
		db = client.Database(name)
	}
	return client, db, err
}

func MongoConnect(ctx context.Context, uri string, retry int32) (*mongo.Client, error) {
	conn, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err := conn.Ping(ctx, nil); err != nil {
		if retry >= 3 {
			return nil, err
		}
		retry = retry + 1
		time.Sleep(time.Second * 2)
		return MongoConnect(ctx, uri, retry)
	}
	return conn, err
}
