package core

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"task-service/model"
	"time"
)

func HmacSha256(data string, secret string) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}

func Signature() (string, string, string) {
	id := os.Getenv("YT_ID")
	if id == "" {
		id = "21139"
	}
	key := os.Getenv("YT_KEY")
	if key == "" {
		key = "MWNhYzEwNWViOWY3NDRlNDlkYzZiYTZmYjdhNWExNjY="
	}
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)
	signature := HmacSha256(id+timestamp, key)

	return id, timestamp, signature
}

func YTCreateTranslate(uri string) (*model.YTCreateResponse, error) {
	url := "http://long-asr-prod.yitutech.com/v4/lasr"
	method := "POST"
	payload := strings.NewReader(fmt.Sprintf(`{"audioUrl": "%s"}`, uri))

	timeout := 10 * time.Second
	client := &http.Client{
		Timeout: timeout,
	}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		return nil, err
	}

	id, timestamp, signature := Signature()
	req.Header.Add("x-dev-id", id)
	req.Header.Add("x-request-send-timestamp", timestamp)
	req.Header.Add("x-signature", signature)
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(res.Body)

	if res.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("status code - %v", res.StatusCode))
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var result model.YTCreateResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func YTQueryTranslate(task_id string) (*model.YTQueryResponse, error) {
	url := fmt.Sprintf("http://long-asr-prod.yitutech.com/v4/lasr/%v", task_id)
	method := "GET"

	timeout := 10 * time.Second
	client := &http.Client{Timeout: timeout}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		return nil, err
	}

	id, timestamp, signature := Signature()
	req.Header.Add("x-dev-id", id)
	req.Header.Add("x-request-send-timestamp", timestamp)
	req.Header.Add("x-signature", signature)

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(res.Body)

	if res.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("http status code - %v", res.StatusCode))
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var response model.YTQueryResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}
